//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WeatherCom.Models
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;

    public partial class Employee
    {
        public int EmployeeID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public Nullable<int> AddressID { get; set; }
    
        public virtual Address Address { get; set; }

        public static explicit operator Employee(DbSet<Employee> v)
        {
            throw new NotImplementedException();
        }
    }
}
