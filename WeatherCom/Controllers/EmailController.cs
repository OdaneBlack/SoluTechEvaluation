﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using WeatherCom.Models;
using WeatherCom.Controllers;
using HttpClientSample;

namespace WeatherCom.Controllers
{
    public class EmailController : Controller
    {

        //HttpClient client = new HttpClient();
        private WeatherComEntities db = new WeatherComEntities();
        Random random = new Random();
        
        string emailSubject;
        string emailBody;
        string emailaddress="";
        string weatherMobay = "";
        string weatherKingston = "";
        string rainSub = "Rain Today,Reduce in work hours";
        string drySub = "No Rain Today,Full Hours Required";
        string rainBody = "Sorry for the inconvience, Hope that you remain dry. Please be remained that you are only require to work 4 hours today";
        string dryBody = "There will be no rain today.Please be remained that you are required to work all 8 hours today";


        // GET: Email
        public ActionResult Index()
        {

            //0 is for Dry and 1 Is for rain
            int weatherMobay = random.Next(0, 2);

            int weatherKingston = random.Next(0, 2);

         
            WeatherConditions(weatherMobay, weatherKingston);

            //GetJson();
                    
            return View();
        }

        [HttpPost]
        public ActionResult SendEmail()
        {
         
            return View();
        }

        [HttpPost]
        public void Email(string address,string subject, string body)
        {
            try
            {
                //Configuring webMail class to send emails  
                //gmail smtp server  
                WebMail.SmtpServer = "smtp.gmail.com";
                //gmail port to send emails  
                WebMail.SmtpPort = 587;
                WebMail.SmtpUseDefaultCredentials = true;
                //sending emails with secure protocol  
                WebMail.EnableSsl = true;
                //EmailId used to send emails from application  
                WebMail.UserName = "odaneblackz@gmail.com";
                WebMail.Password = "Change Email And Password";

                //Sender email address.  
                WebMail.From = "odaneblackz@gmail.com";
                
                //Send email  
                WebMail.Send(to: address, subject: subject, body: body, isBodyHtml: true);
                //ViewBag.Status = "Email Sent Successfully.";
            }
            catch (Exception)
            {
                //ViewBag.Status = "Problem while sending email, Please check details.";

            }
        }

        public void WeatherConditions(int mobay, int kingston)
        {
            int count = 0;
            //Employee employees = (Employee)db.Employees.Find(1);
            Employee employeesKingston1 = (Employee)db.Employees.Find(1);//Kingston,Manufacturer
            Employee employeesKingston2 = (Employee)db.Employees.Find(2);//Kingston,IT Staff 
            Employee employeesMobay1 = (Employee)db.Employees.Find(15);//Mobay,IT Staff  
            Employee employeesMobay2 = (Employee)db.Employees.Find(25);//Mobay,Manufacturer

          

            if (mobay == 1 && kingston == 1)
            {
                ViewData["WeatherMobay"] = "Rain";
                ViewData["WeatherKingston"] = "Rain";
                emailaddress = employeesKingston1.Email;
                Email(emailaddress, rainSub, rainBody);
                count = count + 1;

                //Senf to IT staff
                if (count == 2)
                {


                }
            }
            if (mobay == 0 && kingston == 0)
            {

                ViewData["WeatherMobay"] = "Sunny";
                ViewData["WeatherKingston"] = "Sunny";
                
                    Employee employees = (Employee)db.Employees.Find(1);
                    emailaddress = employees.Email;
                    count = count + 1;
                    Email(emailaddress, drySub, dryBody);
               
                

            }
            if (mobay == 1 && kingston == 0)
            {

                ViewData["WeatherMobay"] = "Rain";
                ViewData["WeatherKingston"] = "Sunny";
                for (int c=0; c<26;c=c+5)
                {
                    Employee employees = (Employee)db.Employees.Find(c);
                    Address address = db.Addresses.Find(employees.AddressID);
                    emailaddress = employees.Email;
                    count = count + 1;
                    if (address.Parish == "Saint James" )
                    {
                        Email(emailaddress, rainSub, rainBody);
                    }
                    else
                    {
                        Email(emailaddress, drySub, dryBody);
                    }
                   
                }
                //Senf to IT staff
                if (count == 2)
                {


                }
            }
            if (mobay == 0 && kingston == 1)
            {

                ViewData["WeatherMobay"] = "Sunny";
                ViewData["WeatherKingston"] = "Rain";
                for (int c = 0; c < 26; c = c + 5)
                {
                    Employee employees = (Employee)db.Employees.Find(c);
                    Address address = (Address)db.Addresses.Find(employees.AddressID);
                    emailaddress = employees.Email;
                    count = count + 1;
                    if (address.Parish == "Saint James")
                    {
                        Email(emailaddress, drySub, dryBody);
                    }
                    else
                    {
                        Email(emailaddress, rainSub, rainBody);
                    }
                    
                }
                //Senf to IT staff
                if (count == 2)
                {


                }
            }
        }

        public void GetJson()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:64687/");
            client.DefaultRequestHeaders.Accept.Add
                (
                    new MediaTypeWithQualityHeaderValue("application/json")
                );

            HttpResponseMessage response = client.GetAsync("http://api.openweathermap.org/data/2.5/weather?lat=18.47&lon=-77.92&appid=00908372fee629a31d41724934511749").Result;

            if (response.IsSuccessStatusCode)
            {
                ViewData["Json"] = response.Content.ReadAsAsync<Weather>().Result;
            }
        }
        public class Weather
        {
            public int Id { get; set; }
            public string Main { get; set; }
            public string Description { get; set; }
            public string Icon { get; set; }

            
        }
    }
}