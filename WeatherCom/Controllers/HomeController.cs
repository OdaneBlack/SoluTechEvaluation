﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;

namespace WeatherCom.Controllers
{
    public class HomeController : Controller
    {
        coord weather = new coord();
        public ActionResult Index()
        {
            
            weather = GetJsonMonBay();
            var lat = weather.lat;
            var lon = weather.lon;

        
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public coord GetJsonMonBay()
        {

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:64687/");
            client.DefaultRequestHeaders.Accept.Add
                (
                    new MediaTypeWithQualityHeaderValue("application/json")
                );

            HttpResponseMessage response = client.GetAsync("http://api.openweathermap.org/data/2.5/weather?lat=18.47&lon=-77.92&appid=00908372fee629a31d41724934511749").Result;

            if (response.IsSuccessStatusCode)
            {
               weather= response.Content.ReadAsAsync<coord>().Result;
                
            }
            return weather;
        }
        public class coord
        {
            //public int Id { get; set; }
            public int lon { get; set; }
            public int lat { get; set; }
           // public string Icon { get; set; }
        }
    }
}


//http://api.openweathermap.org/data/2.5/forecast?lat=18.47&lon=-77.92&appid=00908372fee629a31d41724934511749   (montego)

//http://api.openweathermap.org/data/2.5/forecast?lat=18&lon=-76.79&appid=00908372fee629a31d41724934511749  (Kinston)